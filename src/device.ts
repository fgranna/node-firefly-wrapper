import { AxiosInstance, AxiosRequestConfig } from 'axios';

interface iPacket {
  ack?: boolean;
  bandwidth: number;
  codr: string;
  device_eui: string;
  fopts: string;
  fcnt: number;
  freq: number;
  gwrx: {
    tmst: number;
    time: string;
    rssi: number;
    lsnr: number;
    gweui: string;
  }[];
  modu: string;
  mtype: string;
  payload_encrypted: boolean;
  payload: string | Buffer;
  port: number;
  received_at: string | Date;
  size: number;
  spreading_factor: number;
  uid: string;
}

export default class Device {
  constructor(private _eui: string, private _interactor: AxiosInstance) {}

  show() {
    return this._interactor.get(`devices/eui/${this._eui}`)
      .then((response) => response.data)
      .catch((e) => console.error(e));
  }

  packets(): Promise<iPacket[]> {
    const config: AxiosRequestConfig = {
      params: {
        //payload_only: 'true'
      }
    };
    return this._interactor.get(`devices/eui/${this._eui}/packets`, config)
      .then((response) => {
        return response.data['packets'].map((packet: iPacket) => {
          packet.received_at = new Date(packet.received_at);
          packet.payload = new Buffer((packet.payload as string), 'hex');
          return packet;
        });
      })
      .catch((e) => console.error(e));
  }

}