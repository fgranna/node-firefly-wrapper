import axios, {AxiosRequestConfig} from 'axios';
import * as https from 'https';

type tInteractorOptions = {
  version: String
}

export function interactor(apiKey: string, version: string = 'v1') {
  const interactorInstance = axios.create({
    baseURL: `https://api.fireflyiot.com/api/${version}/`,
    httpsAgent: new https.Agent({ keepAlive: true })
  });

  interactorInstance.interceptors.request.use((config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
    config.params = config.params || {};
    config.params['auth'] = apiKey;
    return Promise.resolve(config);
  });

  return interactorInstance;
}