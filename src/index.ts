import * as axios from 'axios';
import { interactor } from './interactor';
import Device from './device';

type tFireflyOptions = {}

export  class Firefly {

  private _interactor: axios.AxiosInstance;

  constructor(apiKey: string, opts?: tFireflyOptions) {
    this._interactor = interactor(apiKey);
  }

  device(eui: string) {
    return new Device(eui, this._interactor);
  }

  devices() {
    return this._interactor.get('devices')
      .then((response) => response.data)
      .catch((e) => console.error(e));
  }

  deviceStat(eui: String) {
    return this._interactor.get(`statistics/devices/${eui}`)
      .then((response) => response.data)
      .catch((e) => console.error(e));
  }

  deviceMacEvents(eui: String) {
    return this._interactor.get(`devices/eui/${eui}/mac_events`)
      .then((response) => response.data)
      .catch((e) => console.error(e));
  }



  deviceClass(id: number) {
    return this._interactor.get(`device_classes/${id}`)
      .then((response) => {
        const deviceClass = response.data['device_class'];
        deviceClass.updated_at = new Date(deviceClass.updated_at);
        deviceClass.inserted_at = new Date(deviceClass.inserted_at);
        return deviceClass;
      })
      .catch((e) => console.error(e));
  }

  deviceClassDevices(id: number) {
    return this._interactor.get(`device_classes/${id}/euis`)
      .then((response) => {
        return response.data['devices'];
      })
      .catch((e) => console.error(e));
  }


  deviceClasses() {
    return this._interactor.get('device_classes')
      .then((response) => {
        return response.data['device_classes'].map((deviceClass: any) => {
          deviceClass.updated_at = new Date(deviceClass.updated_at);
          deviceClass.inserted_at = new Date(deviceClass.inserted_at);
          return deviceClass;
        });
      })
      .catch((e) => console.error(e));
  }
}